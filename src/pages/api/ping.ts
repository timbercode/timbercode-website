import { NextApiRequest, NextApiResponse } from "next";

export default (req: NextApiRequest, res: NextApiResponse) => {
  res.setHeader("Content-Type", "text/plain");
  res.status(200);
  res.send("pong");
};
