import React from "react";
import { NextPage } from "next";

const PingPage: NextPage = function () {
  return <p>pong</p>;
};

export default PingPage;
