#!/usr/bin/env bash
set -eo pipefail

NOW_JSON=$1
# To generate Zeit token go to https://zeit.co/account/tokens . This token is
#  required to deploy app to Zeit without need for interactive login process.
ZEIT_TOKEN=$2

if [[ -z "${NOW_JSON}" ]]; then
    echo "Missing path to now.*.json descriptor!"
    exit 1
fi

if [[ -z "${ZEIT_TOKEN}" ]]; then
    echo "Missing Zeit token!"
    exit 1
fi

# Clean-up previous Zeit Now project linking, if any
rm -rf ./.now/

# `--confirm` below is required for non-interactive project linking.
#   See: https://zeit.co/docs/now-cli#commands/now/project-linking
./node_modules/.bin/now deploy \
    --local-config=${NOW_JSON} \
    --scope timbercode \
    --prod \
    --confirm \
    --force \
    --token ${ZEIT_TOKEN}
