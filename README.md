# timbercode-website

https://timbercode.me

## Quick Start

In console:

1. Install [Yarn](https://yarnpkg.com/).
1. Install [nvm](https://github.com/nvm-sh/nvm).
1. Go to the directory where you checked out this repository.
1. Run `nvm install` to set up proper version of Node.js.
1. Run `yarn install` to install external dependencies
1. Run `yarn run dev` to start app in development mode and serve it on [http://localhost:3000](http://localhost:3000).
1. Open [http://localhost:3000](http://localhost:3000) in your web browser.

## Code style

- Use `yarn run format:check` to check whether files are formatted in consistent manner.
- Use `yarn run format` to format files in consistent manner.

## Production Build

To create and serve production build run:

1. `yarn run build`
1. `yarn run start`

Now you can visit [http://localhost:3002](http://localhost:3002) in your web browser.

## Deployment

Production build is deployed by GitLab CI job (defined in [.gitlab-ci.yml](./.gitlab-ci.yml)) from git tags. It requires manual creation of a tag, then manual click on GitLab job to start it. In result app is available under [https://timbercode.me](https://timbercode.me), which is hosted on Zeit as [`timbercode-website-prod` project](https://zeit.co/timbercode/timbercode-website-prod).

It is also possible to deploy from any branch to [https://preview.timbercode.me](https://preview.timbercode.me). Corresponding project on Zeit is named [`timbercode-website-preview` project](https://zeit.co/timbercode/timbercode-website-preview).

You can mimic Zeit configuration while developing locally. To do so run `./node_modules/.bin/now login` to authenticate, then `./node_modules/.bin/now` to link codebase to `timbercode-website-local` Zeit project, and in the end `yarn run dev:now` to serve the app in dev mode on [http://localhost:3001](http://localhost:3001).

This deployment setup is inspired by [https://github.com/UnlyEd/next-typescript-api-zeit-boilerplate](https://github.com/UnlyEd/next-typescript-api-zeit-boilerplate) and it offers more control over workflow than default Zeit <> GitLab integration.

## JetBrains WebStorm

- This project uses [Prettier](https://prettier.io/) for code formatting. You can use "Prettier" plugin bundled with WebStorm. Moreover there is a key shortcut configured by default which allows to reformat whole file at once. For more info please see [https://prettier.io/docs/en/webstorm.html#webstorm-20181-and-above](https://prettier.io/docs/en/webstorm.html#webstorm-20181-and-above).
